import { shallowMount } from "@vue/test-utils";
import TheFooter from "@/components/layout/TheFooter.vue";

describe("TheFooter.vue", () => {
  it("renders title", () => {
    const wrapper = shallowMount(TheFooter, {});
    expect(wrapper.text()).toMatch("Footer");
  });
});
