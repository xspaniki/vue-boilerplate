import { shallowMount } from "@vue/test-utils";
import TheHeader from "@/components/layout/TheHeader.vue";

describe("TheHeader.vue", () => {
  it("renders title", () => {
    const wrapper = shallowMount(TheHeader, {});
    expect(wrapper.text()).toMatch("Header");
  });
});
