1. Libs
   1. _Vuex_ - state managment
      1. https://vuex.vuejs.org/
   2. _Vue-router_ - routing
      1. https://router.vuejs.org/
   3. _Vue-i18n_ - localisations
      1. https://kazupon.github.io/vue-i18n/
2. Plugins
   1. _Sentry_ - error logging
      1. https://docs.sentry.io/platforms/javascript/
   2. _Lodash_ - JS utillities
      1. https://lodash.com/
   3. _Axios_ - HTTP client
      1. https://github.com/axios/axios
   4. Vuelidate - form validations
      1. https://vuelidate.js.org/#getting-started
   5. Vueawesome - font awesome
      1. https://github.com/Justineo/vue-awesome#vue-awesome
3. Tests
   1. _Jest_ - unit
      1. https://jestjs.io/docs/en/getting-started
   2. _Cypress_ - e2e
      1. https://docs.cypress.io/
