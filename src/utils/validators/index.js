import UsersAPI from "@/api/users";

const isUniqueUserBy = (value, attribute) => {
  return UsersAPI.check(`?${attribute}=${value}`).catch(() => {});
};

export { isUniqueUserBy };
