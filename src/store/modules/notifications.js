let notificationId = 1;

const module = {
  namespaced: true,

  state: {
    notifications: []
  },

  getters: {},

  mutations: {
    PUSH(state, notification) {
      state.notifications.push({
        ...notification,
        id: notificationId++
      });
    },

    POP(state, notification) {
      state.notifications = state.notifications.filter(
        item => item.id !== notification.id
      );
    }
  },

  actions: {
    add({ commit }, notification) {
      commit("PUSH", notification);
    },

    remove({ commit }, notification) {
      commit("POP", notification);
    }
  }
};

export default module;
