import Vue from "vue";
import AuthAPI from "@/api/auth";
import StorageService from "@/services/StorageService";

const module = {
  state: {
    currentUser: StorageService.getItem("currentUser")
  },

  getters: {
    currentUser(state) {
      return state.currentUser;
    },

    isLoggedIn(state) {
      return !!state.currentUser;
    }
  },

  actions: {
    login({ commit, dispatch }, payload) {
      return AuthAPI.login(payload)
        .then(response => {
          commit("SET_USER_DATA", response.data);

          const notification = {
            severity: "success",
            message: Vue.$i18n.t("auth.login.notifications.success")
          };

          dispatch("notifications/add", notification, { root: true });

          Vue.$router.push({ name: "profile" });
        })
        .catch(error => {
          if (error.response && error.response.status === 401) {
            const notification = {
              severity: "failure",
              message: Vue.$i18n.t("auth.login.notifications.failure")
            };

            dispatch("notifications/add", notification, { root: true });
          }
        });
    },

    logout({ commit }) {
      return AuthAPI.logout().then(() => {
        commit("UNSET_USER_DATA");
      });
    },

    register({ commit, dispatch }, payload) {
      return AuthAPI.register(payload)
        .then(response => {
          commit("SET_USER_DATA", response.data);

          const notification = {
            severity: "success",
            message: Vue.$i18n.t("auth.register.notifications.success")
          };

          dispatch("notifications/add", notification, { root: true });

          Vue.$router.push({ name: "profile" });
        })
        .catch(() => {});
    },

    requestPasswordReset({ dispatch }, payload) {
      return AuthAPI.requestPasswordReset(payload)
        .then(() => {
          const notification = {
            severity: "success",
            message: Vue.$i18n.t(
              "auth.requestPasswordReset.notifications.success"
            )
          };

          dispatch("notifications/add", notification, { root: true });
        })
        .catch(error => {
          if (error.response && error.response.status === 404) {
            const notification = {
              severity: "failure",
              message: Vue.$i18n.t(
                "auth.requestPasswordReset.notifications.failure"
              )
            };

            dispatch("notifications/add", notification, { root: true });
          }
        });
    },

    passwordReset({ dispatch }, payload) {
      return AuthAPI.passwordReset(payload)
        .then(() => {
          const notification = {
            severity: "success",
            message: Vue.$i18n.t("auth.passwordReset.notifications.success")
          };

          dispatch("notifications/add", notification, { root: true });
        })
        .catch(error => {
          if (error.response && error.response.status === 404) {
            const notification = {
              severity: "failure",
              message: Vue.$i18n.t("auth.passwordReset.notifications.failure")
            };

            dispatch("notifications/add", notification, { root: true });

            Vue.$router.push({ name: "requestPasswordReset" });
          }
        });
    }
  },

  mutations: {
    SET_USER_DATA(state, payload) {
      state.currentUser = payload.data.attributes;
      StorageService.setItem("csrf", payload.csrf);
      StorageService.setItem("currentUser", state.currentUser);
    },

    UNSET_USER_DATA() {
      StorageService.removeItem("csrf");
      StorageService.removeItem("currentUser");
      location.reload();
    }
  }
};

export default module;
