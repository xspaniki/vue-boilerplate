import Vue from "vue";
import Vuex from "vuex";

import auth from "./modules/auth";
import notifications from "./modules/notifications";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    notifications
  }
});

Vue.$store = store;

export default store;
