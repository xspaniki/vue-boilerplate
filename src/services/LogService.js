import Vue from "vue";

const service = {
  logException(err) {
    if (process.env.NODE_ENV === "development") {
      console.error("Exception:\n", err);
    } else {
      Vue.$sentry.captureException(err);
    }
  }
};

export default service;
