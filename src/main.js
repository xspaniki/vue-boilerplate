import Vue from "vue";
import App from "@/App.vue";
import store from "@/store";
import router from "@/router";
import i18n from "@/locales";

import "@/plugins";
import "@/assets/styles/main.css";

Vue.config.productionTip = false;

// Global registration of Base components
const requireComponent = require.context(
  "./components/base",
  false,
  /Base[A-Z]\w+\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = Vue.$lodash.upperFirst(
    Vue.$lodash.camelCase(
      fileName
        .split("/")
        .pop()
        .replace(/\.\w+$/, "")
    )
  );

  Vue.component(componentName, componentConfig.default || componentConfig);
});
//

new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount("#app");
