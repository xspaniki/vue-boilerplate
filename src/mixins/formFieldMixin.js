import LogService from "@/services/LogService";

export const formFieldMixin = {
  inheritAttrs: false,
  props: {
    value: [String, Number],
    label: {
      type: String,
      default: ""
    },
    vuelidator: {
      type: Object
    }
  },
  computed: {
    listeners() {
      return {
        ...this.$listeners,
        input: this.updateValue
      };
    },

    vuelidateErrors() {
      return this.$lodash.reduce(
        this.vuelidator.$params,
        (result, value, key) => {
          if (this.vuelidator.$dirty && !this.vuelidator[key]) {
            if (this.$te(`shared.validations.${key}`)) {
              let translation = this.$t(`shared.validations.${key}`);
              result.push(translation);
            } else {
              LogService.logException(
                `(${this.$i18n.locale}) Localisation for key: '${key}' doesn't exists!`
              );
            }
          }
          return result;
        },
        []
      );
    },

    errorMessage() {
      return this.vuelidateErrors[0];
    }
  },
  methods: {
    updateValue(event) {
      this.$emit("input", event.target.value);
    },

    touchVuelidate() {
      if (!this.vuelidator) {
        return;
      }

      this.vuelidator.$touch();
    },

    validVuelidateClass() {
      let field = this.vuelidator;
      return !field.$invalid && field.$model && field.$dirty;
    },

    errorVuelidateClass() {
      return this.vuelidator.$error;
    }
  }
};
