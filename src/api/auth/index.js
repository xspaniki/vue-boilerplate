import Vue from "vue";

const api = {
  login(data) {
    return Vue.$axios.plain.post("/auth/login", data);
  },

  logout() {
    return Vue.$axios.secure.delete("/auth/logout");
  },

  register(data) {
    return Vue.$axios.plain.post("/auth/register", data);
  },

  recovery(opts) {
    return Vue.$axios.plain.post("/auth/recovery", {}, opts);
  },

  requestPasswordReset(data) {
    return Vue.$axios.plain.post("/auth/request-password-reset", data);
  },

  passwordReset(data) {
    return Vue.$axios.plain.put("/auth/password-reset", data);
  }
};

export default api;
