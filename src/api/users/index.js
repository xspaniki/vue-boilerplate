import Vue from "vue";

const api = {
  check(query) {
    return Vue.$axios.plain.get("/users/check" + query);
  }
};

export default api;
