import Vue from "vue";
import axios from "axios";
import humps from "humps";
import VueAxios from "vue-axios";
import StorageService from "@/services/StorageService";
import AuthApi from "@/api/auth";

const axiosConfig = {
  baseURL: process.env.VUE_APP_API_URL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json"
  },
  transformResponse: [
    ...axios.defaults.transformResponse,
    data => humps.camelizeKeys(data)
  ],
  transformRequest: [
    data => humps.decamelizeKeys(data),
    ...axios.defaults.transformRequest
  ]
};

const secureAxios = axios.create(axiosConfig);
const plainAxios = axios.create(axiosConfig);

secureAxios.interceptors.request.use(config => {
  const method = config.method.toUpperCase();

  if (method !== "OPTIONS" && method !== "GET") {
    config.headers = {
      ...config.headers,
      "X-CSRF-TOKEN": StorageService.getItem("csrf")
    };
  }

  return config;
});

secureAxios.interceptors.response.use(null, error => {
  if (
    error.response &&
    error.response.config &&
    error.response.status === 401
  ) {
    return AuthApi.recovery({
      headers: { "X-CSRF-TOKEN": StorageService.getItem("csrf") }
    })
      .then(response => {
        StorageService.setItem("csrf", response.data.csrf);

        let retryConfig = error.response.config;
        retryConfig.headers["X-CSRF-TOKEN"] = StorageService.getItem("csrf");

        return plainAxios.request(retryConfig);
      })
      .catch(error => {
        Vue.$store.commit("UNSET_USER_DATA");

        return Promise.reject(error);
      });
  } else {
    return Promise.reject(error);
  }
});

const allAxios = {
  secure: secureAxios,
  plain: plainAxios
};

Vue.use(VueAxios, allAxios);
Vue.$axios = allAxios;
Vue.prototype.$axios = allAxios;
