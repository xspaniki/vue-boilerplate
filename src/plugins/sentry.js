import Vue from "vue";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

Sentry.init({
  environment: process.env.NODE_ENV,
  dsn: process.env.VUE_APP_SENTRY_DNS,
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true,
      logErrors: false
    })
  ]
});

Vue.$sentry = Sentry;
Vue.prototype.$sentry = Sentry;
