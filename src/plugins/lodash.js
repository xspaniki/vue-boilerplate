import Vue from "vue";
import _ from "lodash";

Vue.$lodash = _;
Vue.prototype.$lodash = _;
