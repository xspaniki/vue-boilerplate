import Vue from "vue";
import VueRouter from "vue-router";
import VueMeta from "vue-meta";
import StorageService from "@/services/StorageService";

import Profile from "@/views/Profile";
import Login from "@/views/auth/Login";
import Register from "@/views/auth/Register";
import RequestPasswordReset from "@/views/auth/RequestPasswordReset";
import PasswordReset from "@/views/auth/PasswordReset";
import NotFoundError from "@/views/errors/NotFoundError";
import InternalServerError from "@/views/errors/InternalServerError";

Vue.use(VueRouter);
Vue.use(VueMeta);

const routes = [
  {
    path: "/",
    redirect: { name: "login" }
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
    meta: { requiresAuth: true }
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/request-password-reset",
    name: "requestPasswordReset",
    component: RequestPasswordReset
  },
  {
    path: "/password-reset",
    name: "passwordReset",
    component: PasswordReset
  },
  {
    path: "/404",
    name: "404",
    component: NotFoundError,
    props: true
  },
  {
    path: "/500",
    name: "500",
    component: InternalServerError
  },
  {
    path: "*",
    redirect: { name: "404", params: { resource: "page" } }
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = StorageService.getItem("currentUser");

  if (to.matched.some(record => record.meta.requiresAuth) && !isLoggedIn) {
    next({ name: "login" });
  } else {
    next();
  }
});

Vue.$router = router;

export default router;
