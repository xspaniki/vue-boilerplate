module.exports = {
  transpileDependencies: [/\bvue-awesome\b/],
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
